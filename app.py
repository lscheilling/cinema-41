import users, search, json, sqlite3
from flask import Flask, render_template, request, redirect, url_for, session, abort, flash, send_file, send_from_directory
from flask_mobility import Mobility
from werkzeug.exceptions import HTTPException
from datetime import datetime, timedelta, timezone
from werkzeug.security import check_password_hash, generate_password_hash
from random import randint
import traceback
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
import os, io, pdfkit, base64, png
import pyqrcodeng as qrcode

LAST_UPDATE = datetime.now() - timedelta(30)
MAX_POPULARITY = 1


def verify_database():  # Make sure database has correct tables, if not create them
    db = sqlite3.connect("app.db")  # connect to database
    c = db.cursor()  # start query
    #test_tables(c)  # For testing purposes only
    # Table of client accounts
    c.execute("CREATE TABLE if not exists client_account (username TEXT NOT NULL PRIMARY KEY, fname TEXT NOT NULL, sname TEXT NOT NULL, password TEXT NOT NULL, email TEXT NOT NULL UNIQUE)")
    # Table of staff accounts with different access permissions
    c.execute("CREATE TABLE if not exists staff_account (username TEXT NOT NULL PRIMARY KEY, password TEXT NOT NULL, access_tier INT DEFAULT 0)")
    # Table of all movies
    c.execute("CREATE TABLE if not exists movie_details (id TEXT NOT NULL PRIMARY KEY, name TEXT NOT NULL, thumbnail TEXT NOT NULL, trailer TEXT, description TEXT NOT NULL, popularity INT NOT NULL, rating FLOAT NOT NULL, ageRating TEXT NOT NULL, runtime INT NOT NULL, release TEXT NOT NULL)")
    # Table of all stats with multiple possible values (Genres, Directors, Writers, Lead Actors)
    c.execute("CREATE TABLE if not exists movie_stats (movie TEXT NOT NULL, name TEXT NOT NULL, role TEXT NOT NULL, FOREIGN KEY (movie) REFERENCES movie_details(id))")
    # Table of all screens in cinema with different seat layouts
    c.execute("CREATE TABLE if not exists screens (id TEXT NOT NULL PRIMARY KEY, name TEXT NOT NULL, seats BLOB NOT NULL)")
    # Table of all showings for the cimema
    c.execute("CREATE TABLE if not exists showings (id TEXT NOT NULL PRIMARY KEY, movie TEXT NOT NULL, time DATETIME NOT NULL, screen TEXT NOT NULL, FOREIGN KEY (movie) REFERENCES movie_details(id), FOREIGN KEY (screen) REFERENCES screens(id))")
    # Table of all customer tickets.
    c.execute("CREATE TABLE if not exists tickets (id INTEGER PRIMARY KEY AUTOINCREMENT, owner TEXT NOT NULL, showing TEXT NOT NULL, seat TEXT NOT NULL, type INT NOT NULL, FOREIGN KEY (showing) REFERENCES showings(id))")
    #test_data(c)  # For testing purposes only
    db.commit()  # commit changes
    db.close()  # close connection


def test_tables(c):
    c.execute("DROP TABLE if exists client_account")
    c.execute("DROP TABLE if exists staff_account")
    c.execute("DROP TABLE if exists movie_details")
    c.execute("DROP TABLE if exists movie_stats")
    c.execute("DROP TABLE if exists screens")
    c.execute("DROP TABLE if exists showings")
    c.execute("DROP TABLE if exists tickets")
    c.execute("DROP TABLE if exists users")


def test_data(c):
    # Insert Accounts
    c.execute("INSERT OR REPLACE INTO client_account (username, password, email, fname, sname) VALUES (?, ?, ?, ?, ?)", ("user1", generate_password_hash("password"), "user@gmail.com","Bob","Ross"))  # A basic user
    c.execute("INSERT OR REPLACE INTO staff_account (username, password, access_tier) VALUES (?, ?, ?)", ("staff_low", generate_password_hash("password"), 0,))  # Staff with low permissions, ie: a worker
    c.execute("INSERT OR REPLACE INTO staff_account (username, password, access_tier) VALUES (?, ?, ?)", ("staff_med", generate_password_hash("password"), 1,))  # Staff with med permissions, ie: a manager
    c.execute("INSERT OR REPLACE INTO staff_account (username, password, access_tier) VALUES (?, ?, ?)", ("staff_high", generate_password_hash("password"), 2,)) # Staff with high permissions, ie: a administrator

    # Insert Movies
    sample_mov = ("spiderman3", "Spider-Man: Into the Spider-Verse", "spiderman_thumbnail.jpg", "spiderman_trailer.mp4",'“Spider-Man: Into The Spider-Verse” introduces Brooklyn teen Miles Morales, and the limitless possibilities of the Spider-Verse, where more than one can wear the mask.', 0, 8.4, "PG", 117, "12/12/2018",)
    sample_mov_stats = {"movie": "spiderman3", "Genre": ["Animation", "Action", "Adventure"], "Director": ["Bob Persichetti", "Peter Ramsey"], "Writer": ["Phil Lord", "Rodney Rothman"], "Lead Actor": ["Shameik Moore", "Jake Johnson", "Hailee Steinfeld", "Mahershala Ali", "Brian Tyree Henry", "Lily Tomlin", "Luna Lauren Velez", "Zoë Kravitz", "John Mulaney", "Kimiko Glenn", "Nicolas Cage", "Kathryn Hahn", "Liev Schreiber", "Chris Pine", "Natalie Morales"]}
    sample_mov2 = ("httyd", "How to Train Your Dragon", "https://live.staticflickr.com/4114/4859887273_e0c9c92b18_c.jpg",'Young Viking Hiccup Horrendous Haddock the Third (voice of Jay Baruchel) is sent to the Isle of Berk in the North Sea, where he is to subdue a dragon as a rite of passage.', 0, 8.1, "PG", 98, "31/03/2010",)
    sample_mov2_stats = {"movie": "httyd", "Genre": ["Animation", "Action", "Adventure", "Family", "Fantasy"], "Director": ["Dean DeBlois", "Chris Sanders"], "Writer": ["William Davies", "Dean DeBlois", "Chris Sanders", "Cressida Cowell", "Marc Hyman", "Adam F. Goldberg"], "Lead Actor": ["Jay Baruchel", "Gerard Butler", "Craig Ferguson", "America Ferrera", "Jonah Hill", "Christopher Mintz-Plasse", "T.J. Miller", "Kristen WiigRobin", "Atkin Downes", "Philip McGrade", "Kieron Elliott", "Ashley Jensen", "David Tennant"]}
    allStats = [sample_mov_stats, sample_mov2_stats]
    for i in range(20):
        sample_movi = ("movie"+str(i), "Movie "+str(i), "https://holttribe.com/wp-content/uploads/2020/12/holroyd_polar.jpg",'Sample movie description...', 0, randint(0,10), "12", i*45, "31/03/%s"%(randint(2000,2022)),)
        allStats.append({"movie": "movie"+str(i), "Genre": ["Animation"], "Director": ["Director1"], "Writer": ["Writer1"], "Lead Actor": ["Actor1"]})
        c.execute("INSERT OR REPLACE INTO movie_details (id, name, thumbnail, description, popularity, rating, ageRating, runtime, release) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", sample_movi)  # A sample movie
        for j in range(randint(0,5)):
            c.execute("INSERT OR REPLACE INTO showings (id, movie, time, screen) VALUES (?, ?, ?, ?)", ("s"+str(j)+"movie"+str(i), "movie"+str(i), datetime(randint(2020,2021), randint(3,12), randint(1,30), 13, 30), "screen1",))  # A sample showing
            c.execute("INSERT OR REPLACE INTO tickets (owner, showing, seat, type) VALUES (?, ?, ?, ?)", ("user1", "s0movie"+str(i), "B01", randint(0,2),))  # A sample 'adult' ticket

    c.execute("INSERT OR REPLACE INTO movie_details (id, name, thumbnail, trailer, description, popularity, rating, ageRating, runtime, release) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", sample_mov)  # A sample movie
    c.execute("INSERT OR REPLACE INTO movie_details (id, name, thumbnail, description, popularity, rating, ageRating, runtime, release) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", sample_mov2)  # A sample movie
    for stats in allStats:
        for key in stats.keys():
            if key != "movie":
                for name in stats[key]:
                    c.execute("INSERT OR REPLACE INTO movie_stats (movie, name, role) VALUES (?, ?, ?)", (stats["movie"], name, key))

    # Insert Screens
    # Seat format: [num_rows (max 26), num_cols (max 99), taken_seats (using 3 digit seat code)]
    sample_screen = ("screen1", "Screen A1", json.dumps([10, 10, ["A01", "A10"]]),)
    c.execute("INSERT OR REPLACE INTO screens (id, name, seats) VALUES (?, ?, ?)", sample_screen)  # A sample screen

    # Insert Showings
    c.execute("INSERT OR REPLACE INTO showings (id, movie, time, screen) VALUES (?, ?, ?, ?)", ("s1", "spiderman3", datetime(2021, 4, 9, 11, 30), "screen1",))  # A sample showing
    c.execute("INSERT OR REPLACE INTO showings (id, movie, time, screen) VALUES (?, ?, ?, ?)", ("s2", "httyd", datetime(2020, 3, 1, 12, 30), "screen1",))  # A sample showing
    c.execute("INSERT OR REPLACE INTO showings (id, movie, time, screen) VALUES (?, ?, ?, ?)", ("s3", "httyd", datetime(2021, 3, 3, 13, 30), "screen1",))  # A sample showing
    for i in range(30):
        c.execute("INSERT OR REPLACE INTO showings (id, movie, time, screen) VALUES (?, ?, ?, ?)", ("s"+str(i+4), "httyd", datetime(2021, 3, i+1, 8, 30), "screen1",))  # A sample showing

    # Insert Tickets
    c.execute("INSERT OR REPLACE INTO tickets (owner, showing, seat, type) VALUES (?, ?, ?, ?)", ("user1", "s3", "B01", 0,))  # A sample 'adult' ticket
    for i in range(60):
        c.execute("INSERT OR REPLACE INTO tickets (owner, showing, seat, type) VALUES (?, ?, ?, ?)", ("user1", "s1", "A02", randint(0,2),))  # A sample 'child' ticket


def update_popularity():
    global MAX_POPULARITY
    db = sqlite3.connect("app.db")  # connect to database
    c = db.cursor()  # start query
    c.execute("SELECT id FROM movie_details")  # Get all movies
    movies = c.fetchall()
    for movie in movies:
        c.execute("SELECT COUNT(*) FROM tickets JOIN showings ON showings.id == tickets.showing WHERE showings.time >= datetime('now', '-30 days') AND showings.movie = ?", (movie[0],))
        pop = c.fetchone()[0]
        c.execute("UPDATE movie_details SET popularity = ? WHERE id = ?", (pop, movie[0],))
    MAX_POPULARITY = max(c.execute("SELECT MAX(popularity) FROM movie_details").fetchone()[0],1)
    db.commit()  # commit changes
    db.close()  # close connection


def fix_vals(vals, valTypes):
    newVals = []
    for val in vals:  # Format date correctly
        val = dict(val)
        for valType in valTypes:
            if valType == "time":
                val[valType] = datetime.fromisoformat(val[valType])
        newVals.append(val)
    return newVals


def setup_app():
    verify_database()
    app = Flask(__name__, template_folder='root',
                static_folder='static')  # Create server for webpage
    app.config.from_object(__name__)
    app.config["TEMP"] = __name__ +""
    app.secret_key = "super_random_password"
    app.register_blueprint(users.bp)
    app.register_blueprint(search.bp)
    Mobility(app)
    return app


# update_homepage()
app = setup_app()


@app.route('/')
def homepage():
    # Update popularity every day
    global LAST_UPDATE
    if datetime.now() - LAST_UPDATE >= timedelta(1):
        update_popularity()
        LAST_UPDATE = datetime.now()
        # Remove temp files older than 1 day
        if not os.path.exists("static/temp"):
            os.mkdir("static/temp")
        temp_files = os.listdir("static/temp")
        for file in temp_files:
            if datetime.now(tz=timezone.utc) - datetime.fromtimestamp(os.stat("static/temp/"+file).st_mtime, tz=timezone.utc) >= timedelta(1):
                os.remove("static/temp/"+file)
    db = sqlite3.connect("app.db")  # connect to database
    db.row_factory = sqlite3.Row
    c = db.cursor()  # start query
    c.execute("SELECT movie_details.name, movie_details.thumbnail, showings.movie FROM showings JOIN movie_details ON showings.movie == movie_details.id WHERE time >= datetime('now') GROUP BY movie ORDER BY time ASC LIMIT 20")  # Get next showings
    next_showings = c.fetchall()
    c.execute("SELECT movie_details.name, movie_details.thumbnail, showings.movie FROM showings JOIN movie_details ON showings.movie == movie_details.id WHERE time >= datetime('now') GROUP BY movie ORDER BY movie_details.popularity DESC LIMIT 20")  # Get popular showings
    pop_showings = c.fetchall()
    c.execute("SELECT movie_details.name, movie_details.thumbnail, showings.movie FROM showings JOIN movie_details ON showings.movie == movie_details.id WHERE time >= datetime('now') GROUP BY movie ORDER BY movie_details.rating DESC LIMIT 20")  # Get top rated showings
    top_showings = c.fetchall()
    # Get all movies
    c.execute(
        "SELECT id, name, thumbnail FROM movie_details ORDER BY rating DESC LIMIT 60")
    all_movies = c.fetchall()
    # Get most popular movie
    c.execute("SELECT id, name, thumbnail, trailer FROM movie_details WHERE popularity = (SELECT MAX(popularity) FROM movie_details)")
    top_movie = c.fetchall()[0]
    db.commit()  # commit changes
    db.close()  # close connection
    if 'redirect' in session:
        session.pop('redirect')
    return render_template('/index.html', top_movie=top_movie, next_showings=next_showings, pop_showings=pop_showings, top_showings=top_showings, all=all_movies, js=["showingsCarousel.js", "index.js"], css=["index.css", "showingsCarousel.css", "thumbnailCard.css", "searchBar.css"])


@app.route('/pricing')
def pricing():
    return render_template('/pricing.html')

@app.route('/contact')
def contact():
    return render_template('/contact.html')

@app.route('/about')
def about():
    return render_template('/about.html')

@app.route('/privacy-policy')
def pp():
    return render_template('/privacy_policy.html')

@app.route('/terms-of-service')
def tos():
    return render_template('/tos.html')


@app.route('/movie/<movie_id>')
def movie(movie_id):
    # Update popularity every day
    global LAST_UPDATE
    if datetime.now() - LAST_UPDATE >= timedelta(1):
        update_popularity()
        LAST_UPDATE = datetime.now()
    db = sqlite3.connect("app.db")  # connect to database
    db.row_factory = sqlite3.Row
    c = db.cursor()  # start query
    c.execute("SELECT * FROM movie_details WHERE id = ?",
              (movie_id,))  # Get all Movies
    movies = c.fetchall()
    if len(movies) == 1:  # if movie exists
        movie = dict(movies[0])
        c.execute("SELECT showings.id, showings.time, screens.name FROM showings JOIN screens ON screens.id == showings.screen WHERE movie = ? AND time >= datetime('now') ORDER BY time ASC LIMIT 60", (movie_id,))  # Get next 20 Showings
        movie["showings"] = fix_vals(c.fetchall(), ["time"])
        for item in ["Genre", "Director", "Writer", "Lead Actor"]:
            c.execute("SELECT name FROM movie_stats WHERE movie = ? AND role = ?",
                      (movie_id, item,))  # Get next 20 Showings
            movie[str.lower(item)+"s"] = c.fetchall()
        db.commit()  # commit changes
        db.close()  # close connection
        return render_template('/movie.html', movie=movie, js=["showingsCarousel.js"], css=["movie.css", "showingsCarousel.css"], max_pop=MAX_POPULARITY)
    else:
        db.close()  # close connection
        abort(404, "Movie with id '%s' not found." % (movie_id))

@app.route('/ticket/<ticket_id>')
def ticket(ticket_id):
    if not('logged_in' in session.keys() and session['logged_in']):
        return redirect("/login")
    db = sqlite3.connect("app.db")  # connect to database
    db.row_factory = sqlite3.Row
    c = db.cursor()  # start query
    c.execute("SELECT * FROM tickets WHERE id = ?",(ticket_id,))  # get ticket
    ticket = c.fetchone()
    if not (ticket is None):
        if (session["user"] == ticket["owner"]):
            c.execute("SELECT id, seat, type FROM tickets WHERE showing = ? AND owner = ?",(ticket["showing"],ticket["owner"]))  # get ticket
            tickets_res = c.fetchall()
            tickets = []
            for tick in tickets_res:
                tick = dict(tick)
                tick["qr"] = qrcode.create(tick["id"]).png_data_uri(scale=50, quiet_zone=2)
                tickets.append(tick)
            print(tickets)
            c.execute("SELECT * FROM showings WHERE id = ?",(ticket["showing"],))
            showing = dict(c.fetchall()[0])
            c.execute("SELECT * FROM movie_details WHERE id = ?",(showing["movie"],))
            movie = dict(c.fetchall()[0])
            c.execute("SELECT * FROM screens WHERE id = ?",(showing["screen"],))
            screen = dict(c.fetchall()[0])
            showing["time"] = datetime.fromisoformat(showing["time"])
            generatePDFTicket(render_template('/email.html', showing=showing, movie=movie, screen=screen, tickets=tickets))
            return render_template('/ticket.html', showing=showing, movie=movie, screen=screen, ticket=ticket, qr=qrcode.create(ticket["id"]).png_data_uri(scale=15, quiet_zone=0))
        abort(401, "Ticket not owned")
    db.close()
    abort(404, "Ticket with id '%s' not found." % (ticket_id))

@app.route('/buy/<showing_id>', methods=['POST','GET'])
def buy(showing_id):
    if not ('logged_in' in session and session['logged_in']):
        session['redirect'] = "/buy/"+str(showing_id)
        return redirect(url_for("users.login"))
    db = sqlite3.connect("app.db")  # connect to database
    db.row_factory = sqlite3.Row
    c = db.cursor()  # start query
    c.execute("SELECT * FROM showings WHERE id = ?",
              (showing_id,))  # get showing
    showings = c.fetchall()
    if len(showings) == 1:
        errors = []
        showing = dict(showings[0])
        c.execute("SELECT * FROM movie_details WHERE id = ?",
                  (showing["movie"],))
        movie = dict(c.fetchall()[0])
        c.execute("SELECT * FROM screens WHERE id = ?", (showing["screen"],))
        screen = dict(c.fetchall()[0])
        showing["time"] = datetime.fromisoformat(showing["time"])
        tickets = []
        if request.method == "POST":
            selectedSeats = request.form.getlist('seatcheckbox')
            # Check if seat taken
            for seat in selectedSeats:
                seatS = c.execute("SELECT 1 FROM tickets WHERE showing = ? AND seat = ?",(showing_id,seat)).fetchone()
                if seatS is not None:
                    errors.append("Seat {} is taken".format(seat))
            if len(errors) == 0:
                adults = int(request.form['adults'])
                seniors = int(request.form['seniors'])
                children = 0
                if 'children' in request.form:
                    children = int(request.form['children'])
                for seat in selectedSeats:
                    if adults > 0:
                        c.execute("INSERT OR REPLACE INTO tickets (owner, showing, seat, type) VALUES (?, ?, ?, ?)", (session['user'], showing_id, seat, 0,))
                        tickets.append({"id":c.lastrowid,"seat":seat,"type":0,"qr":qrcode.create(c.lastrowid).png_data_uri(scale=50, quiet_zone=2)})
                        adults -= 1
                    elif seniors > 0:
                        c.execute("INSERT OR REPLACE INTO tickets (owner, showing, seat, type) VALUES (?, ?, ?, ?)", (session['user'], showing_id, seat, 1,))
                        tickets.append({"id":c.lastrowid,"seat":seat,"type":1,"qr":qrcode.create(c.lastrowid).png_data_uri(scale=50, quiet_zone=2)})
                        seniors -= 1
                    else:
                        c.execute("INSERT OR REPLACE INTO tickets (owner, showing, seat, type) VALUES (?, ?, ?, ?)", (session['user'], showing_id, seat, 2,))
                        tickets.append({"id":c.lastrowid,"seat":seat,"type":2,"qr":qrcode.create(c.lastrowid).png_data_uri(scale=50, quiet_zone=2)})
                        children -= 1
                db.commit()  # commit changes
                db.close()  # close connection
                #email: cinema41tickets@gmail.com pass: will_lucas_dante41
                if (session["permissions"] == 0):
                    msgRoot = MIMEMultipart('related')
                    msgRoot['Subject'] = "Tickets for "+movie["name"]
                    msgRoot['From'] = "cinema41tickets@gmail.com"
                    msgRoot['To'] = session['email']
                    msgRoot.preamble = 'This is a multi-part message in MIME format.'

                    msgAlternative = MIMEMultipart('alternative')
                    msgRoot.attach(msgAlternative)

                    msgText = MIMEText('HTML required to view email.')
                    msgAlternative.attach(msgText)

                    msgText = MIMEText(render_template('/email.html', showing=showing, movie=movie, screen=screen, tickets=tickets, email=True),'html')
                    msgAlternative.attach(msgText)

                    for ticket in tickets:
                        img = MIMEImage(base64.b64decode(ticket["qr"][22:].encode("ascii")), filename='img'+str(ticket["id"]))
                        img.add_header('Content-ID', '<img'+str(ticket["id"])+'>')
                        msgRoot.attach(img)

                    s = smtplib.SMTP('smtp.gmail.com', 587)
                    s.starttls()
                    s.login("cinema41tickets@gmail.com", "will_lucas_dante41")
                    s.send_message(msgRoot)
                    s.quit()
                generatePDFTicket(render_template('/email.html', showing=showing, movie=movie, screen=screen, tickets=tickets))
                if session["permissions"] > 0:
                    return redirect("/download_pdf_ticket")
                return render_template('/complete.html', showing=showing, movie=movie, screen=screen, tickets=tickets)
        c.execute("SELECT * FROM tickets WHERE showing = ?", (showing_id,))
        tickets = c.fetchall()
        takenSeats = []
        for ticket in tickets:
            takenSeats.append(ticket["seat"])
        screen["seats"] = json.loads(screen["seats"])
        screen["seats"][2].extend(takenSeats)
        db.close()
        if session["permissions"] > 0:
            return render_template('/staff_buy.html', showing=showing, movie=movie, screen=screen, js=["staff_buy.js"], errors=errors)
        return render_template('/buy.html', showing=showing, movie=movie, screen=screen, js=["buy.js"], errors=errors)
    else:
        db.close()
        abort(404, "Showing with id '%s' not found." % (showing_id))

def generatePDFTicket(html): #pass a html string (ie returned by render_template)
    temp_html = open("static/temp/"+session["user"]+"ticket.html","w")
    temp_html.write(html)
    temp_html.close()
    config = pdfkit.configuration(wkhtmltopdf='wkhtmltopdf\\bin\\wkhtmltopdf.exe')
    pdfkit.from_file("static/temp/"+session["user"]+"ticket.html", "static/temp/"+session["user"]+"ticket.pdf",configuration=config)

@app.route("/download_pdf_ticket")
def download_pdf_ticket():
    return send_file("static/temp/"+session["user"]+"ticket.pdf", as_attachment=True)

@app.errorhandler(Exception)
def error(e):
    code = 500
    message = 'An unexpected error has occurred.'
    if isinstance(e, HTTPException):
        code = e.code
        if hasattr(e, 'description'):
            message = e.description
    else:
        print("Unexpected: %s" % (traceback.format_exc()))
    return render_template('/_error.html', error_code=code, error_message=message)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=int(os.environ.get('PORT', 8080)))
# app.run(debug=True)
