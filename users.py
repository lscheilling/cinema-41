import sqlite3

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, abort
)

from werkzeug.security import check_password_hash, generate_password_hash
from schema import Schema, And, Use, Optional, Regex, SchemaError
from bleach import clean
from datetime import datetime

bp = Blueprint('users', __name__)

def fix_vals(vals, valTypes):
    newVals = []
    for val in vals:  # Format date correctly
        val = dict(val)
        for valType in valTypes:
            if valType == "time":
                val[valType] = datetime.fromisoformat(val[valType])
        newVals.append(val)
    return newVals

def getTickets():
    db = sqlite3.connect("app.db")
    db.row_factory = sqlite3.Row
    tickets = db.execute('SELECT movie_details.name, showings.time, screens.name AS screen, tickets.id, tickets.type, tickets.seat FROM tickets JOIN showings on showings.id == tickets.showing JOIN movie_details ON movie_details.id == showings.movie JOIN screens ON showings.screen == screens.id WHERE tickets.owner = ? ORDER BY showings.time',(session["user"],)).fetchall()
    return fix_vals(tickets, ["time"])

# All links accessable by account page
ACC_PAGES = {"income":[2,"Movie Income","income.html",["searchBar.css"],["search.js","income.js"],None],
            "tickets":[0,"My Tickets","account_tickets.html",None,None,getTickets]} # url-address:[permission_level,readable_address,html_file,css,js,data_func]
LINKS = {}
for key in ACC_PAGES.keys():
    LINKS[key] = [ACC_PAGES[key][0],ACC_PAGES[key][1]]

class UsernameExistsError(Exception):
    def __init__(self, username):
        self.errors = [f'User {username} is already registered.']

class InvalidCredentialsError(Exception):
    def __init__(self):
        self.errors = ['Username or password is incorrect.']

@bp.route('/register', methods=['GET', 'POST'])
def register():
    if 'logged_in' in session.keys() and session['logged_in']:
        return redirect("/account")
    errors = []
    if request.method == 'POST':
         # Allow only inputs in the following form
        schema = Schema({
            'username': And(str, Regex("^[a-zA-Z0-9_-]{3,16}$"), error="Username must be between 3-16 characters and contain only letters, numbers, hyphens and underscores"),
            'password':  And(str, Regex("^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,64}$"), error="Password must be between 8-64 characters and contain capital & lowercase letters and numbers"),
            'email': And(str, Regex("^([a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*\.[a-zA-Z]{2,7})$"), error="Email must be of a valid format"),
            'fname': And(str, lambda s: 64 >= len(s) >= 3, error="First Name(s) must be between 3-64 characters"),
            'sname': And(str, lambda s: 64 >= len(s) >= 3, error="Surname must be between 3-64 characters")})

        try:
            db = sqlite3.connect("app.db")
            db.row_factory = sqlite3.Row

            # Clean and verify all incoming data
            validData = schema.validate(dict(request.form))
            for key in validData.keys():
                validData[key] = clean(validData[key])

            # Check username doesn't exist
            if db.execute('SELECT username FROM client_account WHERE username = ?', (validData["username"],)).fetchone() is not None:
                raise UsernameExistsError(validData["username"])

            # Create user and login
            db.execute('INSERT INTO client_account (username, password, email, fname, sname) VALUES (?, ?, ?, ?, ?)',(validData["username"], generate_password_hash(validData["password"]), validData["email"], validData["fname"], validData["sname"]))
            db.commit()
            url = ""
            if 'redirect' in session:
                url = session['redirect']
            session.clear()
            if url != "":
                session['redirect'] = url
            session['user'] = validData["username"]
            session['fname'] = validData["fname"]
            session['sname'] = validData["sname"]
            session['email'] = validData["email"]
            session['staff'] = False
            session['logged_in'] = True
            session['permissions'] = 0
            if 'redirect' in session:
                url = session['redirect']
                session.pop('redirect')
                return redirect(url)
            return redirect("/account")
        except (UsernameExistsError,SchemaError) as e:
            errors=e.errors
    print(errors) # Errors need implementing into login/register pages
    return render_template('/register.html', errors=errors)


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if 'logged_in' in session.keys() and session['logged_in']:
        return redirect("/account")
    errors = []
    if request.method == 'POST':
        # Allow any string for inputs
        schema = Schema({'username': str,'password': str})

        try:

            url = ""
            if 'redirect' in session:
                url = session['redirect']   

            db = sqlite3.connect("app.db")
            db.row_factory = sqlite3.Row

            # Clean and verify all incoming data
            validData = schema.validate(dict(request.form))
            for key in validData.keys():
                validData[key] = clean(validData[key])

            # Look for client account
            user = db.execute('SELECT * FROM client_account WHERE username = ?', (validData["username"],)).fetchone()

            # If wrong credentials, try Staff acount instead
            if user is None or not check_password_hash(user['password'], validData["password"]):

                # Starting username with 'staff:' guarantees staff login (in the event a client account with same username and password)
                if validData["username"][0:6] == "staff:": 
                    validData["username"] = validData["username"][6:]
                s_user = db.execute('SELECT * FROM staff_account WHERE username = ?', (validData["username"],)).fetchone()

                # Check for wrong credentials
                if s_user is None or not check_password_hash(s_user['password'], validData["password"]):
                    raise InvalidCredentialsError()

                # Login to staff account and start session
                session.clear()
                session['permissions'] = s_user['access_tier'] + 1
            else:
                # Login to client account and start session
                session.clear()
                session['permissions'] = 0
                session['fname'] = user['fname']
                session['sname'] = user['sname']
                session['email'] = user['email']
            if url != "":
                session['redirect'] = url
            session['user'] = validData['username']
            session['logged_in'] = True
            if 'redirect' in session:
                url = session['redirect']
                session.pop('redirect')
                return redirect(url)
            return redirect("/account")
        except (SchemaError,InvalidCredentialsError) as e:
            errors=e.errors
    print(errors) # Errors need implementing into login/register pages
    return render_template('/login.html', errors=errors)

@bp.route('/account')
def account():
    global LINKS
    if not('logged_in' in session.keys() and session['logged_in']):
        return redirect("/login")
    if session['permissions'] == 0:
        return render_template('/client_account.html', pages=LINKS)
    else:
        return render_template('/staff_account.html', pages=LINKS)

@bp.route('/account/<account_page>')
def account_page(account_page):
    global ACC_PAGES, LINKS
    if not('logged_in' in session.keys() and session['logged_in']):
        return redirect("/login")
    data = None
    if account_page in ACC_PAGES.keys():
        if ACC_PAGES[account_page][0] == 0:
            if session['permissions'] > 0:
                abort(401, "Please use your personal account to access this page.")
        else:
            if ACC_PAGES[account_page][0] > session['permissions']:
                abort(401, "Insufficient permissions to access this page.")
    else:
        abort(404)
    if ACC_PAGES[account_page][5] != None:
        data = ACC_PAGES[account_page][5]()
    return render_template(ACC_PAGES[account_page][2], pages=LINKS, css=ACC_PAGES[account_page][3], js=ACC_PAGES[account_page][4], data=data)



@bp.route('/logout')
def logout():
    session['logged_in'] = False
    session.clear()
    return redirect(url_for('homepage'))
