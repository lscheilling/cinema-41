var searchLimit = 50;

function finishedSearch(data) {
    $("#searchRes").html("")
    i = 0
    var elem = ''
    for (const property in data) {
        if (i % 4 == 0) {
            elem += '<div class="holder"><div class="row">'
        }
        elem += `<div class="col-md-3 thumbnailCard"><div class="underneath text-center"><a href="/movie/${property}">MOVIE DETAILS</a></div><article class="title text-center"><h4>${data[property]["name"]}</h4></article><img src="`
        if (data[property]["thumbnail"].slice(0, 4) == "http") {
            elem += data[property]["thumbnail"]
        } else {
            elem += `/static/thumbnails/${data[property]["thumbnail"]}`
        }
        elem += `"alt="${data[property]["name"]} - Thumbnail"></div>`
        if (i % 4 == 3) {
            elem += '</div></div>'
        }
        i += 1
    }
    $("#searchRes").append(elem);
}

function getQuery(q) {
    return (window.location.search.match(new RegExp('[?&]' + q + '=([^&]+)')) || [, null])[1];
}

$(document).ready(function () {
    var query = getQuery("q");
    if (typeof query == "string") {
        $('.searchBox').val(query.replaceAll("%20", " "))
        search();
    }
});