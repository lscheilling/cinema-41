var limit = 2;
var price = 500;
$('input.seat').on('change', function (evt) {
    if ($("input.seat:checked").length >= limit) {
        this.checked = false;
    }
});

$('input.noclick').on('change', function (evt) {
    this.checked = !this.checked;
});

$('input.ticketnum').on('change', function (evt) {
    if (this.value == "") {
        this.value = "0";
    }
    limit = 1;
    var ticketnums = $("input.ticketnum");
    for (var i = 0; i < ticketnums.length; i++) {
        limit += parseInt(ticketnums[i].value);
    }
    var selectedseats = $("input.seat:checked");
    for (var i = 0; i <= selectedseats.length - limit; i++) {
        selectedseats[i].checked = false;
    }
    var adults = document.getElementById("adults");
    var children = document.getElementById("children");
    var seniors = document.getElementById("seniors");
    price = 0;
    price += parseInt(adults.value) * 849;
    price += parseInt(seniors.value) * 499;
    price += parseInt(children.value) * 399;
    var priceTag = document.getElementById("totalprice");
    var pounds = (price / 100) | 0;
    var pence = price - (pounds * 100);
    priceTag.innerText = "Price: £" + pounds + "." + ("0" + pence).slice(-2);

});

function validateForm() {
    var numtickets = 0;
    var ticketinputs = $("input.ticketnum");
    for (var i = 0; i < ticketinputs.length; i++) {
        numtickets += parseInt(ticketinputs[i].value);
    }
    var selectedseats = $("input.seat:checked");
    if (selectedseats.length < numtickets) {
        alert("Must select a seat for every ticket");
        return false;
    }

    return confirm("Confirm purchase for seats")
}