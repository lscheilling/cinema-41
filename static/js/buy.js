var limit = 2;
var price = 500;
$('input.seat').on('change', function (evt) {
    if ($("input[class='seat']:checked").length >= limit) {
        this.checked = false;
    }
});

$('input.noclick').on('change', function (evt) {
    this.checked = !this.checked;
});

$('input.ticketnum').on('change', function (evt) {
    if (this.value == "") {
        this.value = "0";
    }
    limit = 1;
    var ticketnums = $("input.ticketnum");
    for (var i = 0; i < ticketnums.length; i++) {
        limit += parseInt(ticketnums[i].value);
    }
    var selectedseats = $("input.seat:checked");
    for (var i = 0; i <= selectedseats.length - limit; i++) {
        selectedseats[i].checked = false;
    }
    var adults = document.getElementById("adults");
    var children = document.getElementById("children");
    var seniors = document.getElementById("seniors");
    price = 0;
    price += parseInt(adults.value) * 849;
    price += parseInt(seniors.value) * 499;
    price += parseInt(children.value) * 399;
    var priceTag = document.getElementById("totalprice");
    var pounds = (price / 100) | 0;
    var pence = price - (pounds * 100);
    priceTag.innerText = "Price: £" + pounds + "." + ("0" + pence).slice(-2);

});

$('input.ced').on('input', function (evt) {
    var raw = this.value;
    raw = raw.replace(/[^0-9]/g,'');
    raw.trim();
    if (raw.length >= 3) {
        raw = raw.slice(0, 2) + "/" + raw.slice(2);
    }
    this.value = raw;
});

$('input.ccn').on('input', function (evt) {
    var raw = this.value;
    raw = raw.replace(/[^0-9]/g,'');
    var split = raw.match(/.{1,4}/g);
    if(split == null){
        this.value = "";
        return;
    }
    var processed = "";
    for(chunk of split){
        processed += chunk + " ";
    }
    processed = processed.trim();
    this.value = processed;
});

$('input.cvc').on('input', function (evt) {
    this.value = this.value.replace(/[^0-9]/g,'');
    
});

function validateForm() {
    var numtickets = 0;
    var ticketinputs = $("input.ticketnum");
    for (var i = 0; i < ticketinputs.length; i++) {
        numtickets += parseInt(ticketinputs[i].value);
    }
    var selectedseats = $("input.seat:checked");
    if (selectedseats.length < numtickets) {
        alert("Must select a seat for every ticket");
        return false;
    }
    var ccn = document.getElementById("ccn").value.replace(/\s/g, '');
    var ced = document.getElementById("ced").value;
    var cvc = document.getElementById("cvc").value;

    if ( ccn != "1" && (ccn == "" || ccn.length < 13) ) {
        alert("Invalid Card Number");
        return false;
    }
    if ( cvc != "1" && (cvc == "" || cvc.length < 3) ) {
        alert("Invalid CVC");
        return false;
    }
    if ( ced != "1" && (ced == "" || ced.length < 5) ) {
        alert("Invalid Expriry Date");
        return false;
    }

    return confirm("Confirm purchase for seats")
}