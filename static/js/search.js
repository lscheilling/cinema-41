var searchTimer = 0; // Time since last search
var searchInterval = setInterval(preSearch, 200); // Try to search every .2 seconds
var queueSearch = false;

$(".searchBox").on('input', function () {
    queueSearch = true; // Queue a search and try to execute a search
    preSearch();
});

function preSearch() { // Seach if input changed and has been 2 seconds since last search
    if ($('.searchBox').val()) {
        if (Date.now() - searchTimer >= 2000 && queueSearch == true) { // Limit searches to every 2 seconds
            queueSearch = false;
            searchTimer = Date.now();
            search();
        }
    } else {
        finishedSearch({})
    }
}

function search() {
    var searchTerm = $('.searchBox').val();
    if (searchTerm) {
        if (typeof searchLimit != "number") limit = 10;
        else limit = searchLimit;
        var posting = $.post('/api/search', {
            "term": searchTerm,
            "limit": limit
        });

        posting.done(function (data) {
            if (typeof finishedSearch != "undefined") {
                finishedSearch(data);
            }
        });
    }
}