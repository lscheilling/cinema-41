var tickets = {
    0: 849,
    1: 499,
    2: 399
};
var incomeChart;
var weekChart;
var selectedPeriod = 0;

$(document).ready(function () {
    var posting = $.post('/api/ticketData');
    var posting2 = $.post('/api/weekTicketData');
    var totalIncome = 0;
    var monthIncome = 0;
    var weekIncome = 0;
    var futureIncome = 0;
    incomeChart = new Chart(document.getElementById('incomeChart').getContext('2d'), {
        type: 'line',
        data: {
            datasets: []
        },
        options: {
            scales: {
                yAxes: [{
                    scaleLabel: {
                        labelString: 'Income (£)',
                        display: true
                    },
                    gridLines: {
                        drawBorder: false
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        labelString: 'Day',
                        display: true
                    },
                    gridLines: {
                        drawBorder: false,
                    },
                    type: 'time',
                    time: {
                        unit: 'day',
                        tooltipFormat: 'll',
                    },
                    ticks: {
                        min: moment().hour(12).minute(0).second(0).startOf("isoWeek").subtract(1, 'year'),
                        max: moment().hour(12).minute(0).second(0).startOf("isoWeek").add(1, 'day').add(6, 'week'),
                        source: 'data'
                    }
                }]
            },
            legend: {
                labels: {
                    filter: function (item, chart) {
                        return (item.text.slice(-3) == "(W)" && selectedPeriod == 0) || (item.text.slice(-3) == "(M)" && selectedPeriod == 1)
                    }
                }
            }
        }
    });
    weekChart = new Chart(document.getElementById('weekChart').getContext('2d'), {
        type: 'line',
        data: {
            datasets: []
        },
        options: {
            scales: {
                yAxes: [{
                    scaleLabel: {
                        labelString: 'Income (£)',
                        display: true
                    },
                    gridLines: {
                        drawBorder: false
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        labelString: 'Day',
                        display: true
                    },
                    gridLines: {
                        drawBorder: false,
                    },
                    type: 'time',
                    time: {
                        unit: 'day',
                        tooltipFormat: 'll',
                    },
                    ticks: {
                        source: 'data'
                    }
                }]
            }
        }
    });

    posting.done(function (data) {
        addData("All Movies", data["All Movies"])
        now = new Date()
        for (i in data["All Movies"]) {
            date = new Date(data["All Movies"][i]["time"]).getTime()
            if (date >= now) {
                futureIncome += tickets[data["All Movies"][i]["type"]]
            } else {
                totalIncome += tickets[data["All Movies"][i]["type"]]
                if (date >= now - (30 * 24 * 60 * 60 * 1000)) {
                    monthIncome += tickets[data["All Movies"][i]["type"]]
                    if (date >= now - (7 * 24 * 60 * 60 * 1000)) {
                        weekIncome += tickets[data["All Movies"][i]["type"]]
                    }
                }
            }
        }
        $(".totalIncome").html((totalIncome / 100).toString());
        $(".weekIncome").html((weekIncome / 100).toString());
        $(".monthIncome").html((monthIncome / 100).toString());
        $(".futureIncome").html((futureIncome / 100).toString());
    });

    posting2.done(function (data) {
        addWeekData(data["result"]);
    });
});

function selectMovie(name, movID) {
    for (i in incomeChart.data.datasets) {
        if (incomeChart.data.datasets[i].name == name) return;
    }
    var posting = $.post('/api/ticketData/' + movID);
    posting.done(function (data) {
        for (const property in data) {
            addData(property, data[property])
        }
    });
}

function updateChart() {
    for (i in incomeChart.data.datasets) {
        const period = incomeChart.data.datasets[i].period
        if (selectedPeriod != period) {
            incomeChart.data.datasets[i].hidden = true
        } else {
            incomeChart.data.datasets[i].hidden = false
        }
    }
    if (selectedPeriod == 0) {
        incomeChart.options.scales.xAxes[0].ticks.min = moment().hour(12).minute(0).second(0).startOf("isoWeek").subtract(1, 'year')
        incomeChart.options.scales.xAxes[0].ticks.max = moment().hour(12).minute(0).second(0).startOf("isoWeek").add(1, 'day').add(20, 'week')
        incomeChart.options.scales.xAxes[0].time.unit = "day"
        incomeChart.options.scales.xAxes[0].time.tooltipFormat = "ll"
        incomeChart.options.scales.xAxes[0].scaleLabel.labelString = "Week"
    } else {
        incomeChart.options.scales.xAxes[0].ticks.min = moment().hour(12).minute(0).second(0).startOf("month").subtract(1, 'year')
        incomeChart.options.scales.xAxes[0].ticks.max = moment().hour(12).minute(0).second(0).startOf("month").add(1, 'day').add(4, 'month')
        incomeChart.options.scales.xAxes[0].time.unit = "month"
        incomeChart.options.scales.xAxes[0].time.tooltipFormat = "MMM YYYY"
        incomeChart.options.scales.xAxes[0].scaleLabel.labelString = "Month"
    }
    incomeChart.update();
}

function getRandomColors() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return [color + "ff", color + "22"];
}

function finishedSearch(data) {
    $("#searchRes").html("")
    for (const property in data) {
        $("#searchRes").append(`<button class="btn btn-secondary ml-1" onclick="selectMovie('${data[property]["name"]}','${data[property]["id"]}')">${data[property]["name"]}</button>`)
    }
}

function addWeekData(data) {
    console.log(data)
    for (i in incomeChart.data.datasets) {
        if (incomeChart.data.datasets[i].name == name) return;
    }
    formattedData = {}
    for (i in data) {
        date = moment(data[i]["time"]).hour(12).minute(0).second(0)
        if (!(data[i]["name"] in formattedData)) {
            colors = getRandomColors();
            formattedData[data[i]["name"]] = {
                label: data[i]["name"],
                backgroundColor: colors[1],
                borderColor: colors[0],
                data: {}
            }
        }
        if (!(date.toString() in formattedData[data[i]["name"]].data)) {
            formattedData[data[i]["name"]].data[date.toString()] = {
                x: date,
                y: 0
            }
        }
        formattedData[data[i]["name"]].data[date.toString()]["y"] += tickets[data[i]["type"]]
    }
    formattedData = Object.values(formattedData);
    for (i in formattedData) {
        var week = moment().hour(12).minute(0).second(0).subtract(7, 'day');
        for (j = 0; j < 8; j++) {
            if (!(week.toString() in formattedData[i].data)) {
                formattedData[i].data[week.toString()] = {
                    x: moment(week),
                    y: 0
                }
            } else {
                formattedData[i].data[week.toString()].y /= 100
            }
            week.add(1, 'day')
        }
        formattedData[i].data = Object.values(formattedData[i].data).sort(function (a, b) {
            return a.x - b.x
        });
        weekChart.data.datasets.push(formattedData[i])
    }
    weekChart.update();
    console.log(formattedData)
}

function addData(name, data) {
    for (i in incomeChart.data.datasets) {
        if (incomeChart.data.datasets[i].name == name) return;
    }
    weekData = {}
    monthData = {}
    for (i in data) {
        date1 = moment(data[i]["time"]).startOf('isoWeek').hour(12).minute(0).second(0)
        date2 = moment(data[i]["time"]).startOf("month").hour(12).minute(0).second(0)
        if (!(date1.toString() in weekData)) {
            weekData[date1.toString()] = {
                x: date1,
                y: tickets[data[i]["type"]]
            }
        } else {
            weekData[date1.toString()].y += tickets[data[i]["type"]]
        }
        if (!(date2.toString() in monthData)) {
            monthData[date2.toString()] = {
                x: date2,
                y: tickets[data[i]["type"]]
            }
        } else {
            monthData[date2.toString()].y += tickets[data[i]["type"]]
        }
    }
    var week = moment().startOf('isoWeek').hour(12).minute(0).second(0).subtract(1, 'year').add(1, 'day')
    var sortedWeekData = [];
    for (var i = 0; i < 73; i++) {
        if (!(week.toString() in weekData)) sortedWeekData.push({
            x: moment(week),
            y: 0
        })
        else sortedWeekData.push({
            x: moment(week),
            y: weekData[week.toString()].y / 100
        })
        week = week.add(1, 'week')
    }
    var month = moment().startOf("month").hour(12).minute(0).second(0).subtract(1, 'year')
    var sortedMonthData = [];
    for (i = 0; i < 17; i++) {
        if (!(month.toString() in monthData)) sortedMonthData.push({
            x: moment(month),
            y: 0
        })
        else sortedMonthData.push({
            x: moment(month),
            y: monthData[month.toString()].y / 100
        })
        month = month.add(1, 'month')
    }
    colors = getRandomColors();
    incomeChart.data.datasets.push({
        label: name + " (M)",
        name: name,
        backgroundColor: colors[1],
        borderColor: colors[0],
        data: sortedMonthData,
        period: 1
    })
    incomeChart.data.datasets.push({
        label: name + " (W)",
        name: name,
        backgroundColor: colors[1],
        borderColor: colors[0],
        data: sortedWeekData,
        period: 0
    })
    updateChart()
}