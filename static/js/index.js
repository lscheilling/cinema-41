$(document).ready(function () {
  $(".topMovie video").click(function () {
    if (this.paused || this.ended) {
      this.play();
    } else {
      this.pause();
    }
  });

  $(".topMovie .mute").click(function () {
    var video = document.getElementsByTagName("video")[0];
    if (video.muted) {
      this.innerHTML = "MUTE";
      video.muted = false;
    } else {
      this.innerHTML = "UNMUTE";
      video.muted = true;
    }
  });
});