import sqlite3

from flask import (
    Blueprint, redirect, render_template, request, session, url_for, abort
)
from bleach import clean
from datetime import datetime

bp = Blueprint('search', __name__)

def fix_vals(vals, valTypes):
    newVals = []
    for val in vals:  # Format date correctly
        val = dict(val)
        for valType in valTypes:
            if valType == "time":
                val[valType] = datetime.fromisoformat(val[valType])
        newVals.append(val)
    return newVals

@bp.route('/api/search', methods=['POST'])
def searchResults(lim=10):
    results = {}
    searchTerm = clean(request.form["term"])
    limit = clean(request.form["limit"])
    try:
        lim = int(limit)
    except:
        pass
    db = sqlite3.connect("app.db")  # connect to database
    db.row_factory = sqlite3.Row
    searchByID = db.execute("SELECT id, name, thumbnail FROM movie_details WHERE id LIKE ? OR name LIKE ? LIMIT ?", (f'%{searchTerm}%',f'%{searchTerm}%',lim,)).fetchall()
    searchByYear = db.execute("SELECT id, name, thumbnail FROM movie_details WHERE substr(release, -4) LIKE ? LIMIT ?", (searchTerm,lim,)).fetchall()
    searchByName, searchByStat = [],[]
    if len(searchTerm) >= 5:
        searchByName = db.execute("SELECT id, name, thumbnail FROM movie_details WHERE description LIKE ? LIMIT ?", (f'%{searchTerm}%',lim,)).fetchall()
        searchByStat = db.execute("SELECT id, movie_details.name, thumbnail FROM movie_stats JOIN movie_details ON movie = id WHERE movie_stats.name LIKE ? LIMIT ?", (f'%{searchTerm}%',lim,)).fetchall()
    db.commit()  # commit changes
    db.close()  # close connection
    for res in [searchByID,searchByYear,searchByName,searchByStat]:
        if len(res) > 0:
            for result in res:
                if not result["id"] in results.keys():
                    results[result["id"]] = dict(result)
                if len(results) >= lim:
                    break
            if len(results) >= lim:
                break
    return results

@bp.route('/search')
def search():
    return render_template('/search.html', js=["search.js","searchPage.js"], css=["thumbnailCard.css","search.css","searchBar.css"])

@bp.route('/api/ticketData/<movie>', methods=['POST'])
def movieTicketData(movie):
    results = {}
    if session['permissions'] >= 2:
        db = sqlite3.connect("app.db")  # connect to database
        db.row_factory = sqlite3.Row
        searchByID = db.execute("SELECT tickets.id, type, time FROM tickets JOIN showings ON showing = showings.id JOIN movie_details ON movie = movie_details.id WHERE movie_details.id = ? AND showings.time >= datetime('now', '-1 year')", (movie,)).fetchall()
        searchName = db.execute("SELECT name from movie_details WHERE id = ?",(movie,)).fetchone()
        db.commit()  # commit changes
        db.close()  # close connection
        results = fix_vals(searchByID, ["time"])
        results = {searchName["name"]:results}
    return results

@bp.route('/api/ticketData', methods=['POST'])
def ticketData():
    results = {}
    if session['permissions'] >= 2:
        db = sqlite3.connect("app.db")  # connect to database
        db.row_factory = sqlite3.Row
        searchByID = db.execute("SELECT tickets.id, type, time FROM tickets JOIN showings ON showing = showings.id WHERE showings.time >= datetime('now', '-1 year')").fetchall()
        db.commit()  # commit changes
        db.close()  # close connection
        results = fix_vals(searchByID, ["time"])
        results = {"All Movies":results}
    return results

@bp.route('/api/weekTicketData', methods=['POST'])
def weekTicketData():
    results = {}
    if session['permissions'] >= 2:
        db = sqlite3.connect("app.db")  # connect to database
        db.row_factory = sqlite3.Row
        searchByID = db.execute("SELECT tickets.id, type, time, movie_details.name FROM tickets JOIN showings ON showing == showings.id JOIN movie_details ON showings.movie == movie_details.id WHERE showings.time >= datetime('now', '-7 days', 'start of day') AND showings.time <= datetime('now', '+1 days', 'start of day')").fetchall()
        db.commit()  # commit changes
        db.close()  # close connection
        for result in results:
            print(result)
        results = fix_vals(searchByID, ["time"])
        results = {"result":results}
    return results
